module bitbucket.org/crazy_genius/easy_proxy

go 1.12

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/sevlyar/go-daemon v0.1.5
	github.com/urfave/cli v1.20.0
	golang.org/x/crypto v0.0.0-20190513172903-22d7a77e9e5f // indirect
	golang.org/x/sys v0.0.0-20190526052359-791d8a0f4d09 // indirect
)
