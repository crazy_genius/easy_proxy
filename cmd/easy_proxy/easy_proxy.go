package main

import (
	"log"
	"os"

	"bitbucket.org/crazy_genius/easy_proxy/internal/app/easy_proxy/command"

	"github.com/sevlyar/go-daemon"
	"github.com/urfave/cli"
)

const currentVersion = "0.1b"

func main() {
	app := cli.NewApp()

	app.Name = "Easy Proxy"
	app.Usage = "Start proxy servie"
	app.Commands = command.Commands

	check(app)

	err := app.Run(os.Args)

	if err != nil {
		log.Fatal(err)
	}
}

func check(app *cli.App) {
	if daemon.WasReborn() {
		appendArgs()
	}
}

func appendArgs() {
	os.Args = append(os.Args, "start")
}
