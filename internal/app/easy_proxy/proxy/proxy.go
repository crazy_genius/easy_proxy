package proxy

import (
	"context"
	"crypto/tls"
	"encoding/base64"
	"fmt"
	"io"
	"net"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/crazy_genius/easy_proxy/internal/app/easy_proxy/core"
)

const currentVersion = "0.1b"

type ProxyServer struct {
	address  string
	port     int
	server   *http.Server
	started  bool
	user     string
	password string
	secured  bool
}

func (ps *ProxyServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	fmt.Printf("New connection:\nHost: %s\nMethod: %s\n", r.URL, r.Method)

	if ps.secured && !isAuth(r, ps.user, ps.password) {
		fmt.Printf("Not authorized request\n")

		requestAuthorization(w)

		return
	}

	if r.Method == http.MethodConnect {
		handleTunneling(w, addWaterMark(r))
	} else {
		handleHTTP(w, addWaterMark(r))
	}
}

func (ps *ProxyServer) Start(in chan core.ControllMessage, out chan core.StatusMessage) {

	for message := range in {
		switch message.Command {
		case "start":
			if ps.started {
				fmt.Printf("Already started ...\n")
				continue
			}
			fmt.Printf("Starting...\n")

			ps.server = &http.Server{
				Addr: fmt.Sprintf("%s:%d", ps.address, ps.port),
				Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					ps.ServeHTTP(w, r)
				}),
				TLSNextProto: make(map[string]func(*http.Server, *tls.Conn, http.Handler)),
				ReadTimeout:  5 * time.Second,
				WriteTimeout: 10 * time.Second,
			}
			go ps.server.ListenAndServe()
			ps.started = true
			out <- core.StatusMessage{Message: "Succsesefuly strted"}
		case "stop":
			if !ps.started || ps.server == nil {
				fmt.Printf("Already stopped ...\n")
				continue
			}

			fmt.Printf("Stopping...\n")

			ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
			defer cancel()

			if err := ps.server.Shutdown(ctx); err != nil {
				out <- core.StatusMessage{Message: err}
			} else {
				out <- core.StatusMessage{Message: "Succsesefuly shoutdown"}
			}

			break
		case "status":
			fmt.Printf("Show status..\n")
			out <- core.StatusMessage{}
		default:

		}
	}
}

func NewProxyServer(address string, port int) *ProxyServer {
	return &ProxyServer{
		started: false,
		address: address,
		port:    port,
		secured: false,
	}
}

func NewSecuredProxyServer(address string, port int, user, password string) *ProxyServer {
	return &ProxyServer{
		started:  false,
		address:  address,
		port:     port,
		secured:  true,
		user:     user,
		password: password,
	}
}

func addWaterMark(r *http.Request) *http.Request {
	r.Header.Set("EasyProxy", fmt.Sprintf("version %s", currentVersion))

	return r
}

func handleTunneling(w http.ResponseWriter, r *http.Request) {
	destConn, err := net.DialTimeout("tcp", r.Host, 10*time.Second)
	if err != nil {
		http.Error(w, err.Error(), http.StatusServiceUnavailable)
		return
	}
	w.WriteHeader(http.StatusOK)
	hijacker, ok := w.(http.Hijacker)
	if !ok {
		http.Error(w, "Hijacking not supported", http.StatusInternalServerError)
		return
	}
	clientConn, _, err := hijacker.Hijack()
	if err != nil {
		http.Error(w, err.Error(), http.StatusServiceUnavailable)
	}

	go transfer(destConn, clientConn)
	go transfer(clientConn, destConn)
}

func transfer(destination io.WriteCloser, source io.ReadCloser) {
	defer destination.Close()
	defer source.Close()
	io.Copy(destination, source)
}

func handleHTTP(w http.ResponseWriter, req *http.Request) {
	resp, err := http.DefaultTransport.RoundTrip(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusServiceUnavailable)
		return
	}
	defer resp.Body.Close()
	copyHeader(w.Header(), resp.Header)
	w.WriteHeader(resp.StatusCode)
	io.Copy(w, resp.Body)
}

func copyHeader(dst, src http.Header) {
	for k, vv := range src {
		for _, v := range vv {
			dst.Add(k, v)
		}
	}
}

func isAuth(r *http.Request, user, password string) bool {
	authHeader := r.Header.Get("Proxy-Authorization")

	splitedHeader := strings.SplitN(authHeader, " ", 2)

	if len(splitedHeader) != 2 {
		return false
	}

	credentials := splitedHeader[1]

	return strings.Compare(credentials, encodeAuth(user, password)) == 0
}

func requestAuthorization(w http.ResponseWriter) {
	w.Header().Add("Proxy-Authenticate", "Basic realm=\"Access to the free internal site\"")
	w.WriteHeader(http.StatusProxyAuthRequired)
	w.Write([]byte("407 Proxy Authentication Required"))
}

func encodeAuth(username, password string) string {
	return base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%s:%s", username, password)))
}
