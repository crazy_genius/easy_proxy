package log

import (
	"os"
	"sync"
	"time"
)

// File struct represent log file
type File struct {
	mu   sync.Mutex
	name string
	file *os.File
}

// NewFile creates a new log File. The file is optional - it will be created if needed.
func NewFile(name string, file *os.File) (*File, error) {
	rw := &File{
		file: file,
		name: name,
	}

	if file == nil {
		if err := rw.Rotate(); err != nil {
			return nil, err
		}
	}

	return rw, nil
}

// Write in log file
func (l *File) Write(b []byte) (n int, err error) {
	l.mu.Lock()
	n, err = l.file.Write(b)
	l.mu.Unlock()
	return
}

// Rotate renames old log file, creates new one, switches log and closes the old file.
func (l *File) Rotate() error {

	if _, err := os.Stat(l.name); err == nil {
		name := l.name + "." + time.Now().Format(time.RFC3339)
		if err = os.Rename(l.name, name); err != nil {
			return err
		}
	}

	file, err := os.Create(l.name)
	if err != nil {
		return err
	}

	l.mu.Lock()
	file, l.file = l.file, file
	l.mu.Unlock()

	if file != nil {
		if err := file.Close(); err != nil {
			return err
		}
	}

	return nil
}
