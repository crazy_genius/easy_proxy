package config

import "github.com/BurntSushi/toml"

//Config a struct represent configuration of proxy service app
type Config struct {
	Proxy            proxyConfig
	Daemon           daemonConfig
	AutoConfigServer autoConfigServer
}

type proxyConfig struct {
	Host     string
	Port     int
	Secured  bool
	User     string
	Password string
}

type daemonConfig struct {
	Log string
	Pid string
}

type autoConfigServer struct {
	Host         string
	ExternalHost string
	Port         int
	Enable       bool
	Exclude      []string
	Include      []string
}

func LoadConfiguration(configurationPath string) (*Config, error) {
	config := &Config{}
	if _, err := toml.DecodeFile(configurationPath, &config); err != nil {
		return nil, err
	}

	return config, nil
}
