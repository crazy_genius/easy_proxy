package web

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/crazy_genius/easy_proxy/internal/app/easy_proxy/core"
)

// AutoConfigurationServer is struct represented service wich provides auto configuration
type AutoConfigurationServer struct {
	http *http.Server

	proxyAddress string
	excludes     []string
	includes     []string

	started bool

	constroll <-chan core.ControllMessage
	message   chan<- core.StatusMessage
}

// NewAutoconfigurationServer create new instance of autoconfiguration service
func NewAutoconfigurationServer(
	host string,
	port int,
	proxyHost string,
	excludes, includes []string,
) *AutoConfigurationServer {

	return &AutoConfigurationServer{
		http: &http.Server{
			Addr:         fmt.Sprintf("%s:%d", host, port),
			ReadTimeout:  1 * time.Second,
			WriteTimeout: 5 * time.Second,
		},
		proxyAddress: proxyHost,
		excludes:     excludes,
		includes:     includes,
		started:      false,
	}
}

// Start execute configuration service
func (ac *AutoConfigurationServer) Start(in <-chan core.ControllMessage, out chan<- core.StatusMessage) {
	out <- core.StatusMessage{Message: "Enable autoconfig proxy service \n"}

	ac.http.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ac.serveHTTP(w, r)
	})

	ac.constroll = in
	ac.message = out

	for msg := range in {
		switch msg.Command {
		case "start":
			if ac.started {
				fmt.Printf("Already started ...\n")
				continue
			}
			fmt.Printf("Autoconfiguration server starting...\n")

			go ac.http.ListenAndServe()
			ac.started = true
			out <- core.StatusMessage{Message: "Autoconfiguration server succsesefuly strted"}
		case "stop":
			if !ac.started || ac.http == nil {
				fmt.Printf("Already stopped ...\n")
				continue
			}

			ac.shutdown(out)

			break
		}
	}
}

func (ac *AutoConfigurationServer) serveHTTP(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/autoconfigure" {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(fmt.Sprintf("Resource %s not found", r.URL.Path)))
		return
	}

	pacFile := `
		// Proxy auto-config file change
		// last edit: %s
		function FindProxyForURL(url, host) {
			%s

			// All other URLs do not require a proxy
			return "DIRECT";
		}
	`

	rules := "\nIncludes:\n"
	for _, include := range ac.includes {
		rule := fmt.Sprintf(`
		if (shExpMatch(host, "%s")) {
			return "PROXY %s";
		}
		`, include, ac.proxyAddress)

		rules += fmt.Sprintf("\n%s\n", rule)
	}

	rules += "Excludes: \n"
	for _, exclude := range ac.excludes {
		rule := fmt.Sprintf(`
		if (shExpMatch(host, "%s")) {
			return "DIRECT";
		}
		`, exclude)

		rules += fmt.Sprintf("\n%s\n", rule)
	}

	t := time.Now()
	pacFile = fmt.Sprintf(
		pacFile,
		fmt.Sprintf("%d-%d-%d %d:%d:%d", t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second()),
		rules)

	w.Header().Set("Contetnt-Type", "application/x-ns-proxy-autoconfig")
	w.Header().Set("Content-Description", "File Transfer")
	w.Header().Set("Content-Disposition", "attachment; filename=config.pac")
	w.Header().Set("Content-Transfer-Encoding", "binary")
	// w.Header().Set("Cache-Control", "must-revalidate")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("Pragma", "public")
	w.Header().Set("Content-Length", fmt.Sprintf("%d", len(pacFile)))

	w.WriteHeader(http.StatusOK)

	fmt.Fprintf(w, pacFile)
}

func (ac *AutoConfigurationServer) shutdown(out chan<- core.StatusMessage) {
	if ac.http != nil {

		out <- core.StatusMessage{Message: "Stopping...\n"}

		ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
		defer cancel()

		if err := ac.http.Shutdown(ctx); err != nil {
			out <- core.StatusMessage{Message: err}
		} else {
			out <- core.StatusMessage{Message: "Succsesefuly shutdown"}
		}

	} else {
		out <- core.StatusMessage{Message: "Server not initialized!\n"}
	}
}
