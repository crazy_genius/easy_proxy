package command

import "github.com/urfave/cli"

//Commands is a list of possible app cli commands
var Commands = []cli.Command{
	startCommand,
	stopCommand,
}
