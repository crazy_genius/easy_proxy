package command

import (
	"fmt"
	"log"
	"os"
	"os/signal"

	"bitbucket.org/crazy_genius/easy_proxy/internal/app/easy_proxy/config"
	"bitbucket.org/crazy_genius/easy_proxy/internal/app/easy_proxy/core"
	"bitbucket.org/crazy_genius/easy_proxy/internal/app/easy_proxy/proxy"

	app_daemon "bitbucket.org/crazy_genius/easy_proxy/internal/app/easy_proxy/daemon"
	app_log "bitbucket.org/crazy_genius/easy_proxy/internal/app/easy_proxy/log"
	"bitbucket.org/crazy_genius/easy_proxy/internal/app/easy_proxy/web"

	"time"

	"github.com/sevlyar/go-daemon"
	"github.com/urfave/cli"
)

var startCommand = cli.Command{
	Name:   "start",
	Usage:  "Starts proxy server",
	Action: startProxyAction,
}

func startProxyAction(ctx *cli.Context) error {

	cfg, err := config.LoadConfiguration("configs/config.toml")

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	cntxt := app_daemon.NewContext(cfg)

	d, err := cntxt.Reborn()

	if err != nil {
		log.Fatal("Unable to run: ", err)
	}

	if d != nil {
		return nil
	}

	defer cntxt.Release()
	log.Print("- - - - - - - - - - - - - - -")
	log.Print("daemon started")

	sig := make(chan os.Signal, 1)

	setupLog(cfg.Daemon.Log)

	go func() {
		signal.Notify(sig, os.Interrupt)
		go startServer(cfg, sig)
	}()

	err = daemon.ServeSignals()

	if err != nil {
		log.Printf("Errr: %s", err.Error())
	}

	s := <-sig

	fmt.Printf("Exit with status code %s \n", s)

	return nil
}

func startServer(cfg *config.Config, sig <-chan os.Signal) {

	fmt.Printf("Server started\n")

	proxyServicecontrollChannel := make(chan core.ControllMessage)
	defer close(proxyServicecontrollChannel)
	proxyServiceMessageChannel := make(chan core.StatusMessage)
	defer close(proxyServiceMessageChannel)

	configServicecontrollChannel := make(chan core.ControllMessage)
	defer close(configServicecontrollChannel)
	configServiceMessageChannel := make(chan core.StatusMessage)
	defer close(configServiceMessageChannel)

	var proxyServer *proxy.ProxyServer

	if cfg.Proxy.Secured == true {
		proxyServer = proxy.NewSecuredProxyServer(
			cfg.Proxy.Host,
			cfg.Proxy.Port,
			cfg.Proxy.User,
			cfg.Proxy.Password,
		)
	} else {
		proxyServer = proxy.NewProxyServer(
			cfg.Proxy.Host,
			cfg.Proxy.Port,
		)
	}

	acs := web.NewAutoconfigurationServer(
		cfg.AutoConfigServer.Host,
		cfg.AutoConfigServer.Port,
		cfg.AutoConfigServer.ExternalHost,
		cfg.AutoConfigServer.Exclude,
		cfg.AutoConfigServer.Include,
	)

	fmt.Println(cfg.AutoConfigServer.ExternalHost)
	// os.Exit(1)?

	go proxyServer.Start(proxyServicecontrollChannel, proxyServiceMessageChannel)
	go acs.Start(configServicecontrollChannel, configServiceMessageChannel)

	go func(proxyMessage, configMessage <-chan core.StatusMessage) {
		for {
			select {
			case pm := <-proxyMessage:
				fmt.Printf("Proxy service:  %#v\n", pm)
			case cm := <-configMessage:
				fmt.Printf("Auto config service: %#v\n", cm)
			}
		}
	}(proxyServiceMessageChannel, configServiceMessageChannel)

	proxyServicecontrollChannel <- core.ControllMessage{Command: "start"}

	if cfg.AutoConfigServer.Enable {
		configServicecontrollChannel <- core.ControllMessage{Command: "start"}
	}

	stop := <-sig

	fmt.Printf("Stop signal code %d recived\n", stop)
	proxyServicecontrollChannel <- core.ControllMessage{Command: "stop"}

	if cfg.AutoConfigServer.Enable {
		configServicecontrollChannel <- core.ControllMessage{Command: "stop"}
	}
}

func setupLog(logFile string) {
	lf, err := app_log.NewFile(logFile, os.Stderr)
	if err != nil {
		log.Fatalf("Unable to create log file: %s", err.Error())
	}

	log.SetOutput(lf)

	rotateLogSignal := time.Tick(24 * time.Hour)
	go func() {
		for {
			<-rotateLogSignal
			if err := lf.Rotate(); err != nil {
				log.Fatalf("Unable to rotate log: %s", err.Error())
			}
		}
	}()
}
