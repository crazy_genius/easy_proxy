package command

import (
	"fmt"
	"os"

	"bitbucket.org/crazy_genius/easy_proxy/internal/app/easy_proxy/config"
	app_daemon "bitbucket.org/crazy_genius/easy_proxy/internal/app/easy_proxy/daemon"
	"github.com/urfave/cli"
)

var stopCommand = cli.Command{
	Name:   "stop",
	Usage:  "Stops proxy server",
	Action: stopProxyAction,
}

func stopProxyAction(ctx *cli.Context) error {
	cfg, err := config.LoadConfiguration("configs/config.toml")

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	cntxt := app_daemon.NewContext(cfg)

	proc, err := cntxt.Search()

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	proc.Signal(os.Kill)

	cntxt.Release()

	return nil
}
