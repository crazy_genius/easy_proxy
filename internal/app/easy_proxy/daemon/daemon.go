package daemon

import (
	"bitbucket.org/crazy_genius/easy_proxy/internal/app/easy_proxy/config"
	"github.com/sevlyar/go-daemon"
)

//NewContext crate configured daemon context
func NewContext(cfg *config.Config) *daemon.Context {

	return &daemon.Context{
		PidFileName: cfg.Daemon.Pid,
		PidFilePerm: 0644,
		LogFileName: cfg.Daemon.Log,
		LogFilePerm: 0640,
		WorkDir:     "./",
		Umask:       027,
		Args:        []string{"[go-daemon easy_proxy]"},
	}
}
