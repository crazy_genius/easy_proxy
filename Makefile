export GO111MODULE=on
GOIMPORTS=goimports
BINARY_NAME=easy_proxy
DOCKER_TAG=`date -u +%Y%m%d`

all: dep clean build
linux: dep build_linux
dep:
	go build -v ./...
build:
	scripts/build.sh debug $(BINARY_NAME)
build_linux:	
	scripts/build.sh debug $(BINARY_NAME) LINUX-64
clean:
	rm -rf $(BINARY_NAME)