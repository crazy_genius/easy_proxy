#!/bin/bash

BUILD_DATE=`date -u +%y%m%d`
BUILD_VERSION=`git describe --always`

if [[ -z $1 ]] || [[ -z $2 ]]; then
    echo "Please provide build mode and output file name" >&2 
    exit 1
fi

if [[ -z $3 ]]; then
    if [[ $OS == "Windows_NT" ]]; then
        GOOS=win32
        if [[ $PROCESSOR_ARCHITEW6432 == "AMD64" ]]; then
            GOARCH=amd64
        else
            if [[ $PROCESSOR_ARCHITECTURE == "AMD64" ]]; then
                GOARCH=amd64
            fi
            if [[ $PROCESSOR_ARCHITECTURE == "x86" ]]; then
                GOARCH=ia32
            fi
        fi
    else
        case `uname -s` in 
            Darwin)
                GOOS=darwin
                ;;
            *)
                GOOS=`uname -s`
                ;;
        esac

        if [[ $GOOS == darwin ]]; then
            case `uname -a` in
                *x86_64)
                    GOARCH=amd64
                    ;;
                *)
                    GOARCH=`uname -p`
                    ;;
            esac
        else
            GOARCH=`uname -p`
        fi
    fi
else
  echo "Explicit selected linux 64"
  if [[ $3 == "LINUX-64" ]]; then
    GOOS=linux
    GOARCH=amd64
  else
    echo "Rquested unsupported fetchure" >&2 
    exit 1
  fi
fi

BUILD_PARAMS_STRING="${BUILD_DATE}-${BUILD_VERSION}-${GOOS}-${GOARCH}"

export GOOS
export GOARCH

if [[ $1 == "debug" ]]; then
    echo "Building development binary..."
    BUILD_PARAMS_STRING="${BUILD_PARAMS_STRING}-DEBUG"
else
    echo "Building production binary..."
fi

go build -ldflags "-X main.version=${BUILD_PARAMS_STRING}" -o $2 cmd/easy_proxy/easy_proxy.go
EXIT_CODE=$?
if [[ $EXIT_CODE == 0 ]]; then
    du -h $2
    echo "Done."
else
    echo "Build fail last exit code: ${EXIT_CODE}." 
fi
