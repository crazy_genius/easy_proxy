# easy_proxy

# build
* Fore buld app use make build
 
# config
For config using .toml file in config dirrectory
example: 
    [proxy]
    host = "0.0.0.0" - wich host should be binded proxy to
    port = 8888 - wich port should use server
    secured = true - should server use basic auth ?
    user = "user" - proxy user name 
    password = "user_pass" - proxy user password
    [daemon]
    log = "/var/log/easy_proxy.log" - log file path
    pid = "/var/run/easy_proxy.pid" - pid file path

# description
Server starts on host:port wich user selected

About log file rotation now we rotete log ever 24 hours